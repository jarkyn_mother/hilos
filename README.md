# README #

¿Para qué este repositorio? ###

* Tienda Magento Hilos en Nogada
* Versión 1.0

### ¿Cómo lo instalo? ###

* Preparar el servidor con las siguientes especificaciones:
* Cualquier distribución Linux x86-64 (puede ser un ubuntu16.04)
* Apache 2.x
* MySQL 5.5 
* PHP 5.6.31

* APACHE
* safe_mode off
* memory_limit > 256MB
* AllowOverride All

* MÓDULOS PHP
* PDO_MySQL
* simplexml
* mcrypt
* hash
* GD
* DOM
* iconv
* curl
* SOAP
* Pcre
* php.ini -> memory_limit=512

* EXTRA
* Crontab

* Configuración de la base de datos en app > etc > local.xml.
* Cambiar las URL de base Magento > core_config_data.
* Configurar la plataforma.

### Pautas de contribución ###

* PRUEBAS
* candelaria.santos@mother.travel

# # # ¿Con quién hablo? ###

* Mother Travel
* david.ramírez@mother.travel